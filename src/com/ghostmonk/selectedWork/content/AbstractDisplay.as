package com.ghostmonk.selectedWork.content 
{	
	import com.ghostmonk.selectedWork.globals.ConfigVariables;
	import com.ghostmonk.selectedWork.globals.PortfolioEvents;
	import com.ghostmonk.selectedWork.utils.EmbeddedTextField;
	import com.greensock.TweenLite;
	
	import flash.display.Sprite;
	import flash.events.Event;

	public class AbstractDisplay extends Sprite 
	{	
		protected var _mainText:EmbeddedTextField;
		protected var _titleText:EmbeddedTextField;
		
		public function AbstractDisplay() 
		{	
			graphics.beginFill( 0x000000, 0.8 );
			graphics.drawRect( 0, 0, 950, 676 );
			graphics.endFill();
			
			_mainText = new EmbeddedTextField( ConfigVariables.HELVETICA_NEUE_LT, 14, 0xFFFFFF );
			_titleText = new EmbeddedTextField( ConfigVariables.HELVETICA_NEUE_LT, 24, 0x79c5fb );
			
			y = -676;	
		}
		
		public function open( tweenObject:Object = null ) : void 
		{	
			if( !tweenObject ) 
			{
				tweenObject = { y:0 };
			}
			TweenLite.to( this, 0.5, tweenObject );	
		} 
		
		public function close():void 
		{	
			TweenLite.to( this, 0.5, { y:-676, onComplete:onCloseComplete } );	
		}
		
		private function onCloseComplete():void 
		{	
			dispatchEvent( new Event( PortfolioEvents.DISPLAY_CLOSED ) );	
		}
	}
}