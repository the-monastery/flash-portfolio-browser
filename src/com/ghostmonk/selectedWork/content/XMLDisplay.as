package com.ghostmonk.selectedWork.content 
{	
	import flash.events.Event;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	
	public class XMLDisplay extends AbstractDisplay 
	{
		public function XMLDisplay( url:String ) 
		{	
			loadData( url );	
		}
		
		private function loadData( url:String ) : void  
		{	
      		var loader:URLLoader = new URLLoader();
      		loader.addEventListener( Event.COMPLETE, onComplete );
      		loader.load( new URLRequest( url ) );	
    	}
    
    	private function onComplete( e:Event ) : void  
		{	
      		e.target.removeEventListener( Event.COMPLETE, onComplete );
      		_mainText.wordWrap = true;
      		_mainText.multiline = true;
      		_mainText.width = 700;
      		_mainText.x = 125;
      		_mainText.y = 60;
      		_mainText.htmlText = e.target.data;
      		addChild( _mainText );	
    	}
	}
}