package com.ghostmonk.selectedWork.content 
{	
	import com.ghostmonk.selectedWork.globals.ConfigVariables;
	import com.ghostmonk.selectedWork.utils.EmbeddedTextField;
	import com.greensock.TweenLite;
	
	import flash.display.Bitmap;
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import flash.net.URLRequest;
	import flash.net.URLVariables;
	import flash.net.sendToURL;
	import flash.text.TextFieldAutoSize;
	import flash.text.TextFieldType;
	
	public class ContactDisplay extends AbstractDisplay 
	{	
		private static const _inputWidth:int = 300;
		private static const _inputHeight:int = 23;
		private static const _labelWidth:int = 100;
		private static const _labelHeight:int = 23;
		
		private var _submit:Sprite;
		private var _formContainer:Sprite;
		
		private var _submittedText:EmbeddedTextField;
		private var _sectionTitle:EmbeddedTextField;
		private var _preamble:EmbeddedTextField;
		private var _nameLabel:EmbeddedTextField;
		private var _emailLabel:EmbeddedTextField;
		private var _messageLabel:EmbeddedTextField;
		private var _errorMessage:EmbeddedTextField;
		private var _nameInput:EmbeddedTextField;
		private var _emailInput:EmbeddedTextField;
		private var _messageInput:EmbeddedTextField;
		
		public function ContactDisplay() 
		{	
			_submit = new Sprite();
			_formContainer = new Sprite();
			_submit.addChild( new Bitmap( new SubmitBMD( 0, 0 ) ) );
			_submit.buttonMode = true;
			
			_formContainer.x = 250;
			_formContainer.y = 200;
			
			_submittedText = new EmbeddedTextField( ConfigVariables.HELVETICA_NEUE_LT, 16, 0xFFFFFF );
			_sectionTitle = new EmbeddedTextField( ConfigVariables.HELVETICA_NEUE_LT, 24, 0x79c5fb );
			_preamble = new EmbeddedTextField( ConfigVariables.HELVETICA_NEUE_LT, 16, 0xFFFFFF );
			_nameLabel = new EmbeddedTextField( ConfigVariables.HELVETICA_NEUE_LT, 16, 0xFFFFFF );
			_emailLabel = new EmbeddedTextField( ConfigVariables.HELVETICA_NEUE_LT, 16, 0xFFFFFF );
			_messageLabel = new EmbeddedTextField( ConfigVariables.HELVETICA_NEUE_LT, 16, 0xFFFFFF );
			_errorMessage = new EmbeddedTextField( ConfigVariables.HELVETICA_NEUE_LT, 16, 0xa02626, false, false, false );
			_nameInput = new EmbeddedTextField( ConfigVariables.HELVETICA_NEUE_LT, 16, 0x79c5fb, true, false, false );
			_emailInput = new EmbeddedTextField( ConfigVariables.HELVETICA_NEUE_LT, 16, 0x79c5fb, true, false, false );
			_messageInput = new EmbeddedTextField( ConfigVariables.HELVETICA_NEUE_LT, 16, 0x79c5fb, true );
			
			setTextField( _nameLabel, 0, 60, _labelWidth, _labelHeight, "Name:" );
			setTextField( _sectionTitle, 80, 50, _labelWidth, _labelHeight, "Contact" );
			setTextField( _emailLabel, 0, _nameLabel.y + 40, _labelWidth, _labelHeight, "Email:" );
			setTextField( _messageLabel, 0, _emailLabel.y + 40, _labelWidth, _labelHeight, "Message:" );
			setTextField( _nameInput, _nameLabel.x + _labelWidth + 20, _nameLabel.y, _inputWidth, _inputHeight, "", true );
			setTextField( _emailInput, _emailLabel.x + _labelWidth + 20, _emailLabel.y, _inputWidth, _inputHeight, "", true );
			setTextField( _messageInput, _messageLabel.x + _labelWidth + 20, _messageLabel.y, _inputWidth, 140, "", true, 500 );
			setTextField( _preamble, -150, -100, 750, 120, ConfigVariables.CONTACT_PREAMBLE );
			setTextField( _submittedText, _formContainer.x + 125, _formContainer.y + 50, _inputWidth, 200 );
			
			_submit.x = _messageInput.x + 100;
			_submit.y = _messageInput.y + 160;
			
			setTextField( _errorMessage, _messageInput.x, _submit.y + 50, _labelWidth, _labelHeight );
			
			_submittedText.htmlText = ConfigVariables.SUBMITTED_TEXT;
			_submittedText.alpha = 0;
			
			addChild( _submittedText );
			addChild( _sectionTitle );
			_formContainer.addChild( _submit );
			addChild( _formContainer );
			
			_submit.addEventListener( MouseEvent.CLICK, submitForm );

		}
		
		private function setTextField( object:EmbeddedTextField, xPos:Number, yPos:Number, w:Number, h:Number, tex:String = "", input:Boolean = false, mChar:int = 50 ) : void 
		{		
		 	if( input ) 
			{		
		 		object.autoSize = TextFieldAutoSize.NONE;
		 		object.type = TextFieldType.INPUT;
		 		object.maxChars = mChar;
		 		object.border = true;
		 		object.borderColor = 0xFFFFFF;	
		 	}
		 	
		 	object.x = xPos;
		 	object.y = yPos;
		 	object.width = w;
		 	object.height = h;
		 	object.text = tex;
		 	
		 	_formContainer.addChild( object ); 	
		}
		
		private function submitForm( e:MouseEvent ) : void 
		{	
			var address:String = _emailInput.text;
			var isValidAddress:Boolean = true;
			var atTracker:Number = 0;
			var dotTracker:Number = 0;
			var i:Number = 0;
			_errorMessage.text = "";
			
			while( i <= address.length - 1 ) 
			{	
				if( address.charAt(0)== "@" || address.charAt(0)== "." || address.charAt( address.length - 1 ) == "@" || address.charAt( address.length - 1 ) == "." ) 
				{
					isValidAddress = false;
					break;
				}
	
				address.charAt(i) == "@" ? atTracker++ : "";
				address.charAt(i) == "." ? dotTracker++ : "";
				atTracker == 1 && dotTracker == 1 ? isValidAddress = true : isValidAddress = false;
	
				i++;
			}
			
			_nameInput.borderColor = 0xFFFFFF;
			_emailInput.borderColor = 0xFFFFFF;
			_messageInput.borderColor = 0xFFFFFF;
			
            var request:URLRequest = new URLRequest( "form.php" );
            var variables:URLVariables = new URLVariables();
			
			if( _nameInput.text == "" ) 
			{	
				stage.focus = _nameInput;
				_nameInput.borderColor = 0xa02626;
				_errorMessage.text = "* All Fields are Required";
				
			}
			else if( _emailInput.text == "" ) 
			{	
				stage.focus = _emailInput;
				_emailInput.borderColor = 0x870404;
				_errorMessage.text = "* All Fields are Required";
				
			}
			else if( !isValidAddress ) 
			{	
				stage.focus = _emailInput;
				_emailInput.borderColor = 0xa02626;
				_errorMessage.text = "* Invalid Email";
				
			}
			else if( _messageInput.text == "" ) 
			{	
				stage.focus = _messageInput;
				_messageInput.borderColor = 0xa02626;
				_errorMessage.text = "* All Fields are Required";
				
			}
			else 
			{	
				variables.name = _nameInput.text;
            	variables.email = _emailInput.text;
				variables.message = _messageInput.text;
            	request.data = variables;
            	sendToURL( request );
				
				TweenLite.to( _formContainer, 0.5, { alpha:0, onComplete:removeForm } );
				_submit.removeEventListener( MouseEvent.CLICK, submitForm );
				_submit.buttonMode = false;	
			}
		}
		
		private function removeForm() : void 
		{	
			removeChild( _formContainer );
			TweenLite.to( _submittedText, 0.5, { alpha:1 } );	
		}
		
		public function resetForm() : void 
		{	
			_submit.addEventListener( MouseEvent.CLICK, submitForm );
			addChild( _formContainer );
			_submit.buttonMode = true;
			_formContainer.alpha = 1;
			_submittedText.alpha = 0;	
		}
	}
}