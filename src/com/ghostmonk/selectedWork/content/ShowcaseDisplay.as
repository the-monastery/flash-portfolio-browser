package com.ghostmonk.selectedWork.content 
{	
	import caurina.transitions.Equations;
	import caurina.transitions.Tweener;
	
	import com.ghostmonk.selectedWork.controllers.ShowcaseController;
	import com.ghostmonk.selectedWork.globals.ClientData;
	import com.ghostmonk.selectedWork.globals.ConfigVariables;
	import com.ghostmonk.selectedWork.globals.PortfolioEvents;
	import com.ghostmonk.selectedWork.utils.EmbeddedTextField;
	import com.ghostmonk.selectedWork.utils.StillImageLoader;
	import com.greensock.TweenLite;
	
	import flash.display.Bitmap;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.text.TextField;
	
	public class ShowcaseDisplay extends AbstractDisplay 
	{
		private var _showcaseController:ShowcaseController;
		
		private var _projectTitle:EmbeddedTextField;
		private var _technology:EmbeddedTextField;
		private var _description:EmbeddedTextField;
		private var _exLink:EmbeddedTextField;
		
		private var _image1:StillImageLoader;
		private var _image2:StillImageLoader;
		
		private var _next:Sprite;
		private var _previous:Sprite;
		private var _switcher:Sprite;
		
		private var _currentNode:int;
		
		public function ShowcaseDisplay( controller:ShowcaseController ) 
		{	
			_showcaseController = controller;
			
			_projectTitle = new EmbeddedTextField( ConfigVariables.HELVETICA_NEUE_LT, 24, 0x79c5fb );
			_technology = new EmbeddedTextField( ConfigVariables.HELVETICA_NEUE_LT, 16, 0xFFFFFF );
			_description = new EmbeddedTextField( ConfigVariables.HELVETICA_NEUE_LT, 16, 0xFFFFFF );
			_exLink = new EmbeddedTextField( ConfigVariables.HELVETICA_NEUE_LT, 16, 0xFFFFFF );
			
			_image1 = new StillImageLoader();
			_image2 = new StillImageLoader();
			
			_next = new Sprite();
			_next.addChild( new Bitmap( new NextBMD( 0, 0 ) ) );
			
			_previous = new Sprite();
			_previous.addChild( new Bitmap( new PreviousBMD( 0, 0 ) ) );
			
			_switcher = new Sprite();
			_switcher.addChild( new Bitmap( new SwitcherBMD( 0, 0 ) ) );
			
			var uiBgImage:Bitmap = new Bitmap( new UIBackgroundBMD( 0, 0 ) );
			
			_showcaseController.addEventListener( PortfolioEvents.SHOWCASE_CLICK, onShowcaseClick );
			
			_next.addEventListener( MouseEvent.CLICK, onNextProject );
			_previous.addEventListener( MouseEvent.CLICK, onPreviousProject );
			_switcher.addEventListener( MouseEvent.CLICK, switchImages );
			
			addDisplayItem( _projectTitle, 80, 70 );
			_projectTitle.width = 800;
			addDisplayItem( uiBgImage, ConfigVariables.BODY_WIDTH - uiBgImage.width - 35, _projectTitle.y );
			addDisplayItem( _previous, uiBgImage.x + 15, uiBgImage.y + 10, true );
			addDisplayItem( _next, _previous.x + _previous.width + 15, _previous.y, true );
			
			addDisplayItem( _image1, _projectTitle.x, _projectTitle.y + 65 );
			addDisplayItem( _image2, _image1.x + 100, _image1.y + 100 );
			swapChildren( _image1, _image2 );
			
			addDisplayItem( _technology, 600, _image1.y );
			_technology.width = 300;
			_description.width = _technology.width;
			_exLink.width = _technology.width;
			addDisplayItem( _switcher, 500, _image1.y, true );	
		}
		
		private function addDisplayItem( object:*, xPos:Number, yPos:Number, buttonMode:Boolean = false ) : void 
		{	
			addChild(object);
			object.x = xPos;
			object.y = yPos;
			if( buttonMode ) 
			{
				object.buttonMode = true;
			}
		}
		
		private function onShowcaseClick( e:Event ) : void 
		{	
			navigate( _showcaseController.currentIndex );	
		}
		
		private function onNextProject( e:MouseEvent ) : void 
		{	
			_currentNode = _currentNode == ClientData.CLIENT_LIST.length() - 1 ? 0 : _currentNode + 1; 
			navigate( _currentNode );	
		}
		
		private function onPreviousProject( e:MouseEvent ) : void 
		{	
			_currentNode = _currentNode == 0 ? ClientData.CLIENT_LIST.length() - 1 : _currentNode - 1;
			navigate( _currentNode );	
		}
		
		private function switchImages( e:MouseEvent ) : void 
		{	
			//To apply the bezier, I first need to figure out which image is on top
			var top:StillImageLoader = _image1.y < _image2.y ? _image1 : _image2;
			var bottom:StillImageLoader = _image1.y < _image2.y ? _image2 : _image1;
			
			var bottomBezier:Object = [ { x:top.x + 50, y:top.y + 250 }, { x:top.x - 100, y:top.y + 100 } ];
			var topBezier:Object = [ { x:bottom.x - 50, y:bottom.y - 250 }, { x:bottom.x + 100, y:bottom.y - 100 } ];
			
			TweenLite.to( top, 0.5, { x:bottom.x, y:bottom.y, bezier:topBezier, onComplete:enableListener } );
			TweenLite.to( bottom, 0.5, { x:top.x, y:top.y, bezier:bottomBezier } );
			TweenLite.to( this, 0.25, { onComplete:swapDepths } );
			
			top.tweenOverLay( true );
			bottom.tweenOverLay( false );
			_switcher.removeEventListener( MouseEvent.CLICK, switchImages );	
		}
		
		private function enableListener() : void 
		{	
			_switcher.addEventListener( MouseEvent.CLICK, switchImages );	
		}
		
		private function swapDepths() : void 
		{	
			swapChildren( _image1, _image2 );	
		}
		
		private function addAssets( node:Number ) : void 
		{		
			_currentNode = node;
			_next.addEventListener( MouseEvent.CLICK, onNextProject );
			_previous.addEventListener( MouseEvent.CLICK, onPreviousProject );
			var linkTxt:String = ClientData.CLIENT_LIST[ node ].link;
			
			_image1.alpha = 1;
			_image2.alpha = 1;
			
			if( _image1.y < _image2.y ) 
			{	
				_image2.tweenOverLay( true );
				_image1.loadNewImg( ClientData.CLIENT_LIST[ node ].images.@image1 );
				_image2.loadNewImg( ClientData.CLIENT_LIST[ node ].images.@image2 );
				
			}
			else 
			{	
				_image1.tweenOverLay( true );
				_image2.loadNewImg( ClientData.CLIENT_LIST[ node ].images.@image1 );
				_image1.loadNewImg( ClientData.CLIENT_LIST[ node ].images.@image2 );
				
			}
			
			Tweener.addTween( _projectTitle, { _text:( ClientData.CLIENT_LIST[ node ].project ).toString(), transition:Equations.easeNone, time:0.5 } );
			
			var techHTML:String = _technology.htmlText = "<font color='#79c5fb'>Technology Used</font><br />" + ClientData.CLIENT_LIST[ node ].technology;
			Tweener.addTween( _technology, { _text:_technology.text, time:0.5, onComplete:injectHTML, transition:Equations.easeNone, onCompleteParams:[ _technology, techHTML ] } );
			
			var descHTML:String = _description.htmlText = "<font color='#79c5fb'>Description</font><br />" + ClientData.CLIENT_LIST[ node ].description;
			Tweener.addTween( _description, { _text:_description.text, time:1, onComplete:injectHTML, transition:Equations.easeNone, onCompleteParams:[ _description, descHTML ] } );
			
			var linkHTML:String = _exLink.htmlText = linkTxt == "" ? "" : "<font color='#79c5fb'>External Link</font><br /><a href='http://" + linkTxt + "' target='blank'>" + linkTxt + "</a>";
			Tweener.addTween( _exLink, { _text:_exLink.text, time:0.5, onComplete:injectHTML, transition:Equations.easeNone, onCompleteParams:[ _exLink, linkHTML ] } );	
			
			formatShowcase();
			
			_technology.text = "";
			_description.text = "";
			_exLink.text = "";	
		}
		
		private function injectHTML( field:TextField, string:String ) : void 
		{	
			field.htmlText = string;	
		}
		
		public function navigate( node:Number ) : void 
		{	
			_next.removeEventListener( MouseEvent.CLICK, onNextProject );
			_previous.removeEventListener( MouseEvent.CLICK, onPreviousProject );
			
			TweenLite.to( _image2, 0.3, { alpha:0 } );
			TweenLite.to( _image1, 0.35, { alpha:0, onComplete:addAssets, onCompleteParams:[ node ] } );
		}
		
		private function formatShowcase() : void 
		{	
			addDisplayItem( _description, _technology.x, _technology.y + _technology.height + 20 );
			addDisplayItem( _exLink, _technology.x, _description.y + _description.height + 20 );	
		}
	}
}