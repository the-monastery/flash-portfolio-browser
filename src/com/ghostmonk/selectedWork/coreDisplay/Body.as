package com.ghostmonk.selectedWork.coreDisplay 
{	
	import com.ghostmonk.selectedWork.controllers.MainDisplayController;
	import com.ghostmonk.selectedWork.globals.ConfigVariables;
	import com.greensock.TweenLite;
	
	import flash.display.Sprite;
	import flash.events.Event;

	public class Body extends Sprite 
	{
		private var _header:Header;
		private var _borderRight:Sprite;
		private var _borderLeft:Sprite;
		private var _borderBottom:Sprite;
		private var _mainController:MainDisplayController;
		private var _mainDisplay:MainDisplayContainer;
		private var _background:Sprite;
		
		public function Body() 
		{	
			_header = new Header();
			_background = new Sprite();
			_borderBottom = new Sprite();
			_borderLeft = new Sprite();
			_borderRight = new Sprite();
			
			_borderRight.x = ConfigVariables.BODY_WIDTH;
			_borderBottom.y = ConfigVariables.BODY_HEIGHT;
			
			_mainController = new MainDisplayController();
			_mainController.x = ConfigVariables.NAVIGATION_X;
			_mainController.y = ConfigVariables.NAVIGATION_Y;
			_mainDisplay = new MainDisplayContainer(_mainController);
			
			addChild( _background );
			addChild( _mainDisplay );
			addChild( _header );
			addChild( _mainController );
			
			addChild( _borderBottom );
			addChild( _borderLeft );
			addChild( _borderRight );
			
			addEventListener( Event.ADDED_TO_STAGE, onAddedToStage );
			
			drawSolidRect( _background, ConfigVariables.BODY_WIDTH, ConfigVariables.BODY_HEIGHT, 0x000000, 0.5 );
			drawSolidRect( _borderBottom, ConfigVariables.BODY_WIDTH, 1, 0x999999, 1 );
			drawSolidRect( _borderLeft, 1, ConfigVariables.BODY_HEIGHT, 0x999999, 1 );
			drawSolidRect( _borderRight, 1, ConfigVariables.BODY_HEIGHT, 0x999999, 1 );
		}
		
		private function drawSolidRect( sprite:Sprite, w:Number, h:Number, color:Number, alpha:Number ) : void 
		{	
			sprite.graphics.beginFill( color, alpha );
			sprite.graphics.drawRect( 0, 0, w, h );
			sprite.graphics.endFill();	
		}
		
		private function onAddedToStage( e:Event ) : void 
		{	
			_borderBottom.scaleX = 0;
			_borderLeft.scaleY = 0;
			_borderRight.scaleY = 0;
			_background.alpha = 0;
			_header.y = -_header.height;
			TweenLite.to( _background, 0.5, { alpha:1 } );
			TweenLite.to( _borderBottom, 0.5, { scaleX:1 } );
			TweenLite.to( _borderLeft, 0.5, { scaleY:1 } );
			TweenLite.to( _borderRight, 0.5, { scaleY:1 } );
			TweenLite.to( _header, 0.5, { y:0, delay:0.5, onComplete:initDisplay } );	
		}
		
		private function initDisplay() : void 
		{	
			_mainDisplay.y = _header.height + 120;
			_mainDisplay.initContainer();	
		}
	}
}