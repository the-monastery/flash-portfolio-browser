package com.ghostmonk.selectedWork.coreDisplay 
{
	import com.ghostmonk.selectedWork.content.AbstractDisplay;
	import com.ghostmonk.selectedWork.content.ContactDisplay;
	import com.ghostmonk.selectedWork.content.ShowcaseDisplay;
	import com.ghostmonk.selectedWork.content.XMLDisplay;
	import com.ghostmonk.selectedWork.controllers.MainDisplayController;
	import com.ghostmonk.selectedWork.controllers.ShowcaseController;
	import com.ghostmonk.selectedWork.globals.DataPaths;
	import com.ghostmonk.selectedWork.globals.PortfolioEvents;
	
	import flash.display.Sprite;
	import flash.events.Event;

	public class MainDisplayContainer extends Sprite 
	{
		private var _currentDisplay:AbstractDisplay;
		
		private var _contactDisplay:AbstractDisplay;
		private var _showcaseDisplay:AbstractDisplay;
		
		private var _ghostmonkDisplay:AbstractDisplay;
		private var _bioDisplay:AbstractDisplay;
		private var _journalDisplay:AbstractDisplay;
		private var _resumeDisplay:AbstractDisplay;
		
		private var _mainController:MainDisplayController;
		private var _showcaseController:ShowcaseController;
		
		public function MainDisplayContainer( mainController:MainDisplayController ) 
		{
			_mainController = mainController;
			_showcaseController = new ShowcaseController();
			_showcaseController.addEventListener( PortfolioEvents.SHOWCASE_CLICK, onShowcaseClick );
			
			_contactDisplay = new ContactDisplay();
			_showcaseDisplay = new ShowcaseDisplay( _showcaseController );
			
			_ghostmonkDisplay = new XMLDisplay( DataPaths.GHOSTMONK_URL );
			_bioDisplay = new XMLDisplay( DataPaths.BIO_URL );
			_journalDisplay = new XMLDisplay( DataPaths.JOURNAL_URL );
			_resumeDisplay = new XMLDisplay( DataPaths.RESUME_URL );
			
			_mainController.addEventListener( PortfolioEvents.NAVIGATE_CLICK, onNavigateClick );
			_currentDisplay = _showcaseDisplay;
		}
		
		public function initContainer() : void 
		{
			activate( _showcaseDisplay );
			addChild( _showcaseController );
			//add sections in activateContent() after the header and showcaseController have built in... so you don't see their content on build in
			_showcaseDisplay.open( { y:0, delay:0.5, onComplete:activateContent } );
		}
		
		private function activateContent() : void 
		{
			activate( _ghostmonkDisplay );
			activate( _bioDisplay );
			activate( _journalDisplay );
			activate( _resumeDisplay );
			activate( _contactDisplay );
		}
		
		private function activate( sprite:AbstractDisplay ) : void 
		{
			addChildAt( sprite, 0 );
			sprite.addEventListener( PortfolioEvents.DISPLAY_CLOSED, revealNewDisplay );
		}
		
		private function onNavigateClick( e:Event ) : void 
		{
			_currentDisplay.close();
			switch( _mainController.currentSection ) {
				
				case PortfolioEvents.GHOSTMONK_SECTION: 
					_currentDisplay = _ghostmonkDisplay; 	
					break;
					
				case PortfolioEvents.BIO_SECTION: 		
					_currentDisplay = _bioDisplay; 			
					break;
					
				case PortfolioEvents.JOURNAL_SECTION: 	
					_currentDisplay = _journalDisplay; 		
					break;
					
				case PortfolioEvents.RESUME_SECTION: 	
					_currentDisplay = _resumeDisplay; 		
					break;
					
				case PortfolioEvents.CONTACT_SECTION: 	
					_currentDisplay = _contactDisplay; 		
					break;			
			}
			
			_showcaseController.enableControls();
		}
		
		private function onShowcaseClick( e:Event ) : void 
		{
			if( _currentDisplay != _showcaseDisplay ) 
			{
				_currentDisplay.close();
				_currentDisplay = _showcaseDisplay;	
			}	
			
			_mainController.enableControls();
		}
		
		private function revealNewDisplay( e:Event ) : void 
		{
			_currentDisplay.open();
		}
	}
}