package com.ghostmonk.selectedWork.coreDisplay 
{	
	import com.ghostmonk.selectedWork.globals.ConfigVariables;
	import com.ghostmonk.selectedWork.utils.EmbeddedTextField;
	
	import flash.display.Bitmap;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import flash.net.navigateToURL;
	
	public class Header extends Sprite 
	{
		private var _contactText:EmbeddedTextField;
		
		public function Header() 
		{	
			_contactText = new EmbeddedTextField( ConfigVariables.HELVETICA_NEUE_LT, 15, 0xFFFFFF, true, true, true );
			_contactText.width = 300;
			var glowImage:Bitmap = new Bitmap( new GlowBMD( 0, 0 ) );
			var headerImage:Bitmap = new Bitmap( new HeaderBMD( 0, 0 ) );
			var titleImage:Bitmap = new Bitmap( new TitleBMD( 0, 0 ) );
			var aceImage:Bitmap = new Bitmap( new AceBMD( 0, 0 ) );
			
			glowImage.y = 42;
			titleImage.x = 630;
			titleImage.y = 15;
			_contactText.x = 30;
			_contactText.y = 80;
			aceImage.x = 740;
			aceImage.y = 180;
			
			addChild( headerImage );
			addChild( titleImage );
			addChild( glowImage );
			addChild( _contactText );
			addChild( aceImage );
			
			loadContactInfo();
		}
		
		private function loadContactInfo() : void 
		{	
			var urlLoader:URLLoader = new URLLoader( new URLRequest( "xmlData/contactInfo.html" ) );
			urlLoader.addEventListener( Event.COMPLETE, onComplete );
		}
		
		private function onComplete( e:Event ) : void 
		{	
			_contactText.htmlText = e.target.data as String;	
		}
	}
}