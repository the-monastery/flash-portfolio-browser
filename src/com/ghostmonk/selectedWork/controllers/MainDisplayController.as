package com.ghostmonk.selectedWork.controllers 
{	
	import com.ghostmonk.selectedWork.globals.PortfolioEvents;
	import com.ghostmonk.selectedWork.utils.MovieClipButton;
	import com.greensock.TweenLite;
	
	import flash.display.Sprite;
	import flash.events.Event;

	public class MainDisplayController extends Sprite 
	{	
		private var _currentSection:String; 
		
		private var _ghostmonkBtn:MovieClipButton;
		private var _bioBtn:MovieClipButton;
		private var _journalBtn:MovieClipButton;
		private var _resumeBtn:MovieClipButton;
		private var _contactBtn:MovieClipButton;
		
		public function MainDisplayController() 
		{	
			_ghostmonkBtn = new MovieClipButton( new GhostmonkBtn() );
			_bioBtn = new MovieClipButton( new BioBtn() );
			_journalBtn = new MovieClipButton( new JournalBtn() );
			_resumeBtn = new MovieClipButton( new ResumeBtn() );
			_contactBtn = new MovieClipButton( new ContactBtn() );
			
			createAction( _ghostmonkBtn, 0 );
			createAction( _bioBtn, _ghostmonkBtn.x + _ghostmonkBtn.width + 10 );
			createAction( _journalBtn, _bioBtn.x + _bioBtn.width + 10 );
			createAction( _resumeBtn, _journalBtn.x + _journalBtn.width + 10 );
			createAction( _contactBtn, _resumeBtn.x + _resumeBtn.width + 10 );
			
			var mask:Sprite = new Sprite();
			mask.graphics.beginFill( 0x000000 );
			mask.graphics.drawRect( 0, 0, width + 3, height + 3 );
			mask.graphics.endFill();
			addChild( mask );
			this.mask = mask;
			mask.x = -width;
			TweenLite.to( mask, 0.5, { x:0, delay:2.5 } );	
		}
		
		public function get currentSection() : String 
		{	
			return _currentSection;	
		}
		
		private function createAction( button:MovieClipButton, x:Number ) : void 
		{	
			button.addEventListener( PortfolioEvents.ABSTRACT_BTN_CLICK, onBtnClick );
			button.x = x;
			button.mouseChildren = false;
			addChild( button );	
		}
		
		private function onBtnClick( e:Event ) : void 
		{	
			enableControls();
			
			( e.target as MovieClipButton ).disable();
			
			switch( e.target ) {
				
				case _ghostmonkBtn:	
					_currentSection = PortfolioEvents.GHOSTMONK_SECTION;	
					break;
					
				case _bioBtn:		
					_currentSection = PortfolioEvents.BIO_SECTION;			
					break;
					
				case _journalBtn:	
					_currentSection = PortfolioEvents.JOURNAL_SECTION;		
					break;
					
				case _resumeBtn:	
					_currentSection = PortfolioEvents.RESUME_SECTION;		
					break;
					
				case 
					_contactBtn:	
					_currentSection = PortfolioEvents.CONTACT_SECTION;		
					break;
					
			}
			
			dispatchEvent( new Event( PortfolioEvents.NAVIGATE_CLICK ) );	
		}
		
		public function enableControls() : void 
		{	
			_ghostmonkBtn.enable();
			_bioBtn.enable();
			_journalBtn.enable();
			_resumeBtn.enable();
			_contactBtn.enable();	
		}
	}
}