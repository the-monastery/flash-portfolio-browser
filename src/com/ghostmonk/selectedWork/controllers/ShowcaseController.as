package com.ghostmonk.selectedWork.controllers 
{	
	import flash.display.Sprite;
	import com.ghostmonk.selectedWork.utils.ImageLoaderButton;
	import flash.display.Stage;
	import flash.events.Event;
	import com.greensock.TweenLite;
	import flash.events.MouseEvent;
	import com.ghostmonk.selectedWork.globals.ClientData;
	import com.ghostmonk.selectedWork.globals.PortfolioEvents;
	import com.ghostmonk.selectedWork.globals.ConfigVariables;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import com.ghostmonk.selectedWork.globals.DataPaths;
	import com.ghostmonk.selectedWork.utils.BitmapButton;
	import com.ghostmonk.selectedWork.utils.LabelTrack;
	
	public class ShowcaseController extends Sprite 
	{
		private var _thumbContainer:Sprite;
		private var _thumbBtns:Array;
		private var _labelTrack:LabelTrack;
		
		private var _scrollRight:BitmapButton;
		private var _scrollLeft:BitmapButton;
		
		private var _currentIndex:int;
		
		public function ShowcaseController() 
		{	
			_thumbBtns = new Array();
			_thumbContainer = new Sprite();
			_currentIndex = 0;
			_labelTrack = new LabelTrack( 30, _thumbContainer );
			
			_scrollRight = new BitmapButton( new ScrollOnBMD( 0, 0 ), new ScrollOffBMD( 0, 0 ) );
			_scrollLeft = new BitmapButton( new ScrollOnBMD( 0, 0 ), new ScrollOffBMD( 0, 0 ) );
		
			addChild( _labelTrack );
			addChild( _thumbContainer );
			addChild( _scrollLeft );
			addChild( _scrollRight );
			
			_scrollLeft.y = - _scrollLeft.height;
			_scrollRight.rotation = 180;
			_scrollRight.x = ConfigVariables.BODY_WIDTH;
			_thumbContainer.x = _scrollLeft.width;
			
			addEventListener( Event.ADDED_TO_STAGE, onAddedToStage );
			
			_thumbContainer.addEventListener( MouseEvent.MOUSE_MOVE, onMouseMove );
			_thumbContainer.addEventListener( MouseEvent.MOUSE_OUT, onMouseOut );
			_thumbContainer.addEventListener( MouseEvent.ROLL_OVER, onRollOver );
			_thumbContainer.addEventListener( MouseEvent.ROLL_OUT, onRollOut );
			
			var mask:Sprite = new Sprite();
			mask.graphics.beginFill( 0x000000 );
			mask.graphics.drawRect( 0, 0, ConfigVariables.BODY_WIDTH, 100 );
			mask.graphics.endFill();
			addChild( mask );
			_thumbContainer.y = -100;
			mask.y = _thumbContainer.y;
			
			_thumbContainer.mask = mask;
			y = -200;
		}
		
		public function get currentIndex() : int 
		{	
			return _currentIndex;		
		}
		
		public function enableControls() : void 
		{	
			for each(var btn:ImageLoaderButton in _thumbBtns) 
			{
				btn.enable();
			}	
		}
		
		private function onAddedToStage( e:Event ) : void 
		{	
			xmlDataLoader();	
		}
		
		private function xmlDataLoader():void 
		{	
			var xmlLoader:URLLoader = new URLLoader( new URLRequest( DataPaths.CLIENT_WORK ) ); 
			xmlLoader.addEventListener( Event.COMPLETE, xmlLoadComplete );	
		}
		
		private function xmlLoadComplete( e:Event ) : void 
		{	
			var list:XMLList = ClientData.CLIENT_LIST = XML( e.target.data ).children();
			dispatchEvent( new Event( PortfolioEvents.SHOWCASE_CLICK ) );
			
			var xThumbPos:Number = 0;
			
			for( var i:int = 0; i < list.length(); i++ ) 
			{	
				var thumbURL:String = list[i].images.@thumb;
				
				var thumb:ImageLoaderButton = new ImageLoaderButton( thumbURL, i );
				_thumbContainer.addChild( thumb );
				_thumbBtns.push( thumb );
				
				thumb.addEventListener( PortfolioEvents.SHOWCASE_CLICK, onClick );
				thumb.addEventListener( PortfolioEvents.SHOWCASE_ROLLOVER, onRollover );
				
				thumb.x = xThumbPos;
				xThumbPos += 120;
				
			}
			
			( _thumbBtns[ 0 ] as ImageLoaderButton ).disable();
			e.target.removeEventListener( Event.COMPLETE, xmlLoadComplete );
			TweenLite.to( this, 0.5, { y:-20, delay:0.2 } );
		}
		
		private function onClick( e:Event ):void 
		{	
			_currentIndex = ImageLoaderButton( e.target ).index;
			dispatchEvent( new Event( PortfolioEvents.SHOWCASE_CLICK ) );
			enableControls();
			( e.target as ImageLoaderButton ).disable();
			
		}
		
		private function onRollover( e:Event ) : void 
		{	
			_labelTrack.changeLabelText( ClientData.CLIENT_LIST[ ImageLoaderButton( e.target ).index ].@client );	
		}
		
		private function onMouseOut( e:MouseEvent ) : void 
		{	
			_labelTrack.hideLabel();	
		}
		
		private function onRollOver( e:MouseEvent ) : void 
		{	
			_thumbContainer.addEventListener( Event.ENTER_FRAME, onEnterFrame );		
		}
		
		private function onRollOut( e:MouseEvent ):void 
		{	 
			_scrollLeft.onMouseOut();
			_scrollRight.onMouseOut();
			_thumbContainer.removeEventListener( Event.ENTER_FRAME, onEnterFrame );	
		}
		
		private function onEnterFrame( e:Event ) : void 
		{	
			if( mouseX > ConfigVariables.BODY_WIDTH * 0.5 ) 
			{
				_scrollRight.onMouseOver();	
			}
			else 
			{
				_scrollLeft.onMouseOver();
			}
			
			scrollThumbContainer( ( mouseX - ConfigVariables.BODY_WIDTH * 0.5 ) * 0.1 );	
		}
		
		private function onMouseMove( e:MouseEvent ) : void 
		{	
			_labelTrack.positionLabel( mouseX );	
		}
		
		private function scrollThumbContainer( value:Number ) : void 
		{	
			var minValue:Number = ConfigVariables.BODY_WIDTH - _thumbContainer.width;
			var maxValue:Number = _scrollLeft.width;
			_thumbContainer.x = Math.max( minValue, Math.min( _thumbContainer.x -= value, maxValue ) );	
		}
	}
}