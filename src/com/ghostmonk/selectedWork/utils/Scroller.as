package com.ghostmonk.selectedWork.utils 
{	
	import com.ghostmonk.selectedWork.globals.ConfigVariables;
	
	import flash.display.Bitmap;
	import flash.display.DisplayObject;
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import flash.geom.Rectangle;

	public class Scroller extends Sprite 
	{
		private var _labelTrack:Sprite;
		private var _scrollArrowUp:Sprite;
		private var _scrollArrowDown:Sprite;
		private var _scrollerTrack:Sprite;
		private var _scrollSlider:Sprite;
		private var _displayObject:DisplayObject;
		private var _bounder:Rectangle;
		
		public function Scroller( displayObject:DisplayObject ) 
		{	
			_displayObject = displayObject;
			
			_scrollArrowUp = new Sprite();
			_scrollArrowDown = new Sprite();
			_scrollerTrack = new Sprite();
			_scrollSlider = new Sprite();
			_scrollSlider.buttonMode = true;
			
			addChild( _scrollerTrack );
			addChild( _scrollArrowUp );
			addChild( _scrollArrowDown );
			addChild( _scrollSlider );
			
			_scrollArrowUp.rotation = 180;
			_bounder = new Rectangle();
			
			_scrollArrowUp.addChild( new Bitmap( new ArrowBMD( 0, 0 ) ) );
			_scrollArrowDown.addChild( new Bitmap( new ArrowBMD( 0, 0 ) ) );
			_scrollSlider.addChild( new Bitmap( new SliderBMD( 0, 0 ) ) );	
		}
		
		public function formatScroller() : void 
		{	
			this.alpha = 1;
			stage.addEventListener( MouseEvent.MOUSE_WHEEL, onMouseWheel );
			
			_scrollArrowUp.x = stage.stageWidth;
			_scrollArrowUp.y = _scrollArrowUp.height;
			_scrollArrowDown.x = _scrollArrowUp.x - 21; 
			_scrollArrowDown.y = stage.stageHeight - 16; 
			
			_scrollerTrack.graphics.beginFill( 0x133358, 1 );
			_scrollerTrack.graphics.drawRect( 0, 0, 1, stage.stageHeight );
			_scrollerTrack.graphics.endFill();
			
			_scrollerTrack.x = stage.stageWidth - 11;
			_scrollSlider.y = 16;
			_scrollSlider.x = stage.stageWidth - 18;
			
			_bounder.y = 16;
			_bounder.x = stage.stageWidth - 18;
			_bounder.height = ( stage.stageHeight - 32 ) - _scrollSlider.height;
			_bounder.width = 0;
			
			_scrollSlider.addEventListener( MouseEvent.MOUSE_DOWN, startSlideDrag );	
		}
		
		public function destroyScroller() : void 
		{	
			this.alpha = 0;
			_scrollSlider.removeEventListener( MouseEvent.MOUSE_DOWN, startSlideDrag );
			stage.removeEventListener( MouseEvent.MOUSE_WHEEL, onMouseWheel );
			_scrollerTrack.graphics.clear();	
		} 
		
		private function startSlideDrag( event:MouseEvent ) : void 
		{	
			_scrollSlider.startDrag( true, _bounder );
			stage.addEventListener( MouseEvent.MOUSE_MOVE, onMouseMove );
			stage.addEventListener( MouseEvent.MOUSE_UP, stopSlideDrag );	
		}
		
		private function stopSlideDrag( event:MouseEvent ) : void 
		{	
			_scrollSlider.stopDrag();
			stage.removeEventListener( MouseEvent.MOUSE_MOVE, onMouseMove );
			stage.removeEventListener( MouseEvent.MOUSE_UP, stopSlideDrag );	
		}
		
		private function onMouseMove( e:MouseEvent ) : void 
		{	
			onScroll( _scrollSlider.y );	
		}
		
		private function onMouseWheel( e:MouseEvent ) : void 
		{	
			var sliderY:Number =_scrollSlider.y -= 10 * e.delta;
			onScroll( sliderY );	
		}
		
		private function onScroll( sValue:Number ) : void 
		{	
			_scrollSlider.y = Math.max( 16, Math.min( sValue, _bounder.height + 16 ) );
			var displayY:Number = -( _scrollSlider.y - 16 ) / _bounder.height * ( ConfigVariables.BODY_HEIGHT - stage.stageHeight );
			_displayObject.y = Math.min( 0, Math.max( displayY, -( ConfigVariables.BODY_HEIGHT - stage.stageHeight ) ) );	
		}
	}
}