package com.ghostmonk.selectedWork.utils 
{	
	import com.ghostmonk.selectedWork.globals.PortfolioEvents;
	import com.greensock.TweenLite;
	
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;

	public class MovieClipButton extends Sprite 
	{	
		private static const _frameRate:Number = 0.05;
		
		private var _button:MovieClip;
		
		public function MovieClipButton( button:MovieClip ) 
		{	
			_button = button;
			enable();	
		}
		
		public function enable() : void 
		{	
			_button.gotoAndStop( 0 );
			buttonMode = true;
			addChild( _button );
			addEventListener( MouseEvent.MOUSE_OVER, onMouseOver );
			addEventListener( MouseEvent.MOUSE_OUT, onMouseOut );
			addEventListener( MouseEvent.CLICK, onClick );	
		}
		
		public function disable() : void 
		{	
			_button.gotoAndStop( _button.totalFrames );
			buttonMode = false;
			addChild(_button);
			removeEventListener( MouseEvent.MOUSE_OVER, onMouseOver );
			removeEventListener( MouseEvent.MOUSE_OUT, onMouseOut );
			removeEventListener( MouseEvent.CLICK, onClick );	
		}
		
		private function onMouseOut( e:MouseEvent ) : void 
		{	
			var time:Number = _button.currentFrame * _frameRate;
			TweenLite.to( _button, time, { frame:0 } );		
		}
		
		private function onMouseOver( e:MouseEvent ) : void 
		{	
			var time:Number = ( _button.totalFrames - _button.currentFrame ) * _frameRate;
			TweenLite.to( _button, time, { frame:_button.totalFrames } );	
		}
		
		private function onClick( e:MouseEvent ) : void	
		{	
			dispatchEvent( new Event( PortfolioEvents.ABSTRACT_BTN_CLICK ) );	
		}
	}
}