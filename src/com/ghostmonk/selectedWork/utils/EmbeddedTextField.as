package com.ghostmonk.selectedWork.utils 
{
	import flash.text.AntiAliasType;
	import flash.text.GridFitType;
	import flash.text.TextField;
	import flash.text.TextFieldAutoSize;
	import flash.text.TextFormat;
	
	public class EmbeddedTextField extends TextField 
	{
		private var _textFormat:TextFormat;
		
		public function EmbeddedTextField( fontType:String, size:int, color:Number, select:Boolean = false, multi:Boolean = true, wrap:Boolean = true ) 
		{
			_textFormat = new TextFormat( fontType, size, color );
			
			defaultTextFormat = _textFormat;
			selectable = select;
			embedFonts = true;
			antiAliasType = AntiAliasType.ADVANCED;
			gridFitType = GridFitType.PIXEL;
			autoSize = TextFieldAutoSize.LEFT;
			multiline  = multi;
			wordWrap = wrap;
		}
	}
}