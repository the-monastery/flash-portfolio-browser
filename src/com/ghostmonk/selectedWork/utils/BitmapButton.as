package com.ghostmonk.selectedWork.utils 
{
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.Sprite;
	import flash.events.MouseEvent;

	public class BitmapButton extends Sprite 
	{
		private var _overBitmap:Bitmap;
		
		public function BitmapButton( over:BitmapData, base:BitmapData ) 
		{
			_overBitmap = new Bitmap( over );
			addChild( new Bitmap( base ) );
			
			addEventListener( MouseEvent.MOUSE_OVER, onMouseOver );
			addEventListener( MouseEvent.MOUSE_OUT, onMouseOut );
			addChild( _overBitmap );
			buttonMode = true;
			mouseChildren = false;
		}
		
		public function onMouseOut( e:MouseEvent = null ) : void 
		{
			_overBitmap.alpha = 0;
		}
		
		public function onMouseOver( e:MouseEvent = null ) : void 
		{
			_overBitmap.alpha = 1;
		}
	}
}