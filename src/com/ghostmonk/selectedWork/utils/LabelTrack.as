package com.ghostmonk.selectedWork.utils 
{	
	import caurina.transitions.Tweener;
	
	import com.ghostmonk.selectedWork.globals.ConfigVariables;
	import com.greensock.TweenLite;
	
	import flash.display.Sprite;
	
	public class LabelTrack extends Sprite 
	{
		private var _label:EmbeddedTextField;
		private var _xTrackObj:Sprite;
		private var _widthTracker:Number;
		
		public function LabelTrack( height:Number, xTrackObj:Sprite ) 
		{
			_xTrackObj = xTrackObj;
			
			graphics.beginFill( 0x142542, 1 );
			graphics.drawRect( 0, 0, ConfigVariables.BODY_WIDTH, height );
			graphics.endFill();
			
			_label = new EmbeddedTextField( ConfigVariables.HELVETICA_NEUE_LT, 16, 0xFFFFFF );
			_label.wordWrap = _label.multiline = false;
			addChild( _label );
			_label.y = 3;
			_label.alpha = 0;	
		}
		
		public function changeLabelText( text:String ) : void 
		{	
			_label.text = text;
			_widthTracker = _label.width;
			_label.text = "";
			_label.alpha = 0;
			
			Tweener.addTween( _label, { _text:text, time:0.5, alpha:1 } );	
		}
		
		public function positionLabel( xPos:Number ) : void 
		{
			_label.x = Math.min( ConfigVariables.BODY_WIDTH - _widthTracker, Math.max( xPos - _widthTracker * 0.5, 0 ) );	
		}
		
		public function hideLabel():void 
		{	
			TweenLite.to( _label, 0.5, { alpha:0 } );	
		}
	}
}