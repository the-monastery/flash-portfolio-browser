﻿package com.ghostmonk.selectedWork.utils 
{	
	import com.ghostmonk.selectedWork.globals.PortfolioEvents;
	import com.greensock.TweenLite;
	
	import flash.display.Loader;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.events.ProgressEvent;
	import flash.net.URLRequest;
		
	public class ImageLoaderButton extends Sprite 
	{	
		private var _loader:Loader;
		private var _loadProgressBar:LoadProgressMC;
		private var _screen:Sprite;
		
		private var _index:int;
		
		public function ImageLoaderButton( thumb:String, index:int ) 
		{	
			_index = index;
			_loader = new Loader()
			_loadProgressBar = new LoadProgressMC();
			
			_screen = new Sprite();
			
			_loadProgressBar.x = 10;
			_loadProgressBar.y = 30;
			
			addChild(_loader);
			
			_loader.load( new URLRequest( thumb ) );
			
			_loader.contentLoaderInfo.addEventListener( Event.OPEN, onOpen );
			_loader.contentLoaderInfo.addEventListener( ProgressEvent.PROGRESS, onProgress ); 
			_loader.contentLoaderInfo.addEventListener( Event.COMPLETE, onComplete );
			
			enable();	
		}
		
		public function get index() : int 
		{	
			return _index;	
		}
		
		public function enable() : void 
		{	
			onMouseOut( null );
			addEventListener( MouseEvent.MOUSE_OVER, onMouseOver );
			addEventListener( MouseEvent.MOUSE_OUT, onMouseOut );
			addEventListener( MouseEvent.CLICK, onClick );
			buttonMode = true;	
		}
		
		public function disable() : void 
		{	
			removeEventListener( MouseEvent.MOUSE_OVER, onMouseOver );
			removeEventListener( MouseEvent.MOUSE_OUT, onMouseOut );
			removeEventListener( MouseEvent.CLICK, onClick );
			buttonMode = false;
			onMouseOver( null );	
		}
		
		private function onOpen( e:Event ) : void 
		{	
			addChild( _loadProgressBar );
			_loader.contentLoaderInfo.removeEventListener( Event.OPEN, onOpen );		
		}
		
		private function onProgress( e:ProgressEvent ) : void 
		{	
			var percent:Number = int( e.bytesLoaded / e.bytesTotal * 100 );
  			_loadProgressBar.gotoAndStop( percent );	
 		}
		
		private function onComplete( e:Event ) : void 
		{	
			_screen.graphics.beginFill( 0x000000, 0.75 );
			_screen.graphics.drawRect( 0, 0, _loader.content.width, _loader.content.height );
			_screen.graphics.endFill();
			_screen.mouseEnabled = false;
			
			graphics.beginFill( 0x000000, 0 );
			graphics.drawRect( 0, 0, 120, 100 );
			graphics.endFill();
			
			addChild( _screen );
			
			removeChild( _loadProgressBar );
			
			_loader.contentLoaderInfo.removeEventListener( ProgressEvent.PROGRESS, onProgress ); 
			_loader.contentLoaderInfo.removeEventListener( Event.COMPLETE, onComplete );	
		}
		
		private function onMouseOver( e:MouseEvent ) : void 
		{
			TweenLite.to( _screen, 0.5, { y:-100 } );
			dispatchEvent( new Event( PortfolioEvents.SHOWCASE_ROLLOVER ) );	
		}
		
		private function onMouseOut( e:MouseEvent ) : void 
		{
			TweenLite.to( _screen, 0.5, { y:0 } );	
		}
		
		private function onClick( e:MouseEvent ) : void 
		{
			dispatchEvent( new Event( PortfolioEvents.SHOWCASE_CLICK ) );			
		}
	}
}