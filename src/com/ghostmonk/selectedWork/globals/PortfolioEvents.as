package com.ghostmonk.selectedWork.globals 
{
	public class PortfolioEvents 
	{
		public static const NAVIGATE_CLICK:String = "navigateClick";
		public static const ABSTRACT_BTN_CLICK:String = "abstractBtnClick";
		public static const DISPLAY_CLOSED:String = "displayClosed";
		
		public static const SHOWCASE_ROLLOVER:String = "showcaseRollover";
		public static const SHOWCASE_ROLLOUT:String = "showcaseRollout";
		public static const SHOWCASE_CLICK:String = "showcaseClick";
		
		public static const GHOSTMONK_SECTION:String = "ghostmonkSection";
		public static const BIO_SECTION:String = "bioSection";
		public static const JOURNAL_SECTION:String = "journalSection";
		public static const RESUME_SECTION:String = "resumeSection";
		public static const CONTACT_SECTION:String = "contactSection";
	}
}