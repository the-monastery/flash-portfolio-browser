package com.ghostmonk.selectedWork.globals 
{
	public class DataPaths 
	{
		public static const GHOSTMONK_URL:String = "xmlData/ghostmonk.html";
		public static const BIO_URL:String = "xmlData/bio.html";
		public static const JOURNAL_URL:String = "xmlData/journal.html";
		public static const RESUME_URL:String = "xmlData/resume.html";
		public static const CLIENT_WORK:String = "xmlData/clientWork.xml";
	}
}