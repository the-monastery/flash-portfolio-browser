package com.ghostmonk.selectedWork.globals 
{
	public class ConfigVariables 
	{
		public static const HELVETICA_NEUE_LT:String = "HelveticaNeueLT Std Cn";
		public static const BODY_WIDTH:Number = 950;
		public static const BODY_HEIGHT:Number = 1040;
		
		public static const NAVIGATION_X:Number = 545;
		public static const NAVIGATION_Y:Number = 385;
		
		public static const CONTACT_PREAMBLE:String = "Please feel free to send me an email using this form. You may also click the email link in the header which will open up your email client if you have one. I am available for freelance opportunities, contract work, and will even consider full time employment with the right employer. If you would like to know more about me please close this section and check out my biography and resume sections.\n\n I look forward to hearing from you.";
		public static const SUBMITTED_TEXT:String = "<font color='#79c5fb' size='24'>Thank You for your email!</font><br /><br />Your message as been sent to me at <a href='mailto:nicholas@ghostmonk.com?subject=Onerous Rhinoceros Somersault'>nicholas@ghostmonk.com</a>.<br /><br />I will try to contact you as soon as possible.<br /><br />";
	}
}