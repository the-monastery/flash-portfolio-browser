package com.ghostmonk.selectedWork {
		
	import com.ghostmonk.selectedWork.globals.ConfigVariables;
	import com.ghostmonk.selectedWork.utils.EmbeddedTextField;
	import com.ghostmonk.utils.BaseContextMenu;
	import com.ghostmonk.utils.SiteLoader;
	import com.greensock.TweenLite;
	import com.greensock.easing.Quad;
	
	import flash.display.GradientType;
	import flash.display.Loader;
	import flash.display.SpreadMethod;
	import flash.display.Sprite;
	import flash.display.StageAlign;
	import flash.display.StageScaleMode;
	import flash.events.Event;
	import flash.events.ProgressEvent;
	import flash.geom.Matrix;
	import flash.net.URLRequest;


	/**
	 * Standard Site loader with progress bar that loads in Main.swf
	 * 
	 * <p>Additionally, creates a new Flash Contextual menu with links to the author's sites.</p>nb  
	 * 
	 * @author ghostmonk
	 * 
	 */
	public class ProjectLoader extends SiteLoader
	{		
		private var _backgroundGrad:Sprite;
		
		private var _loaderStatus:EmbeddedTextField;
		private var _animatedLoader:LoadingBar;
		
		/**
		 * stage align and scalemode are set and loader is added to the stage. 
		 * 
		 */		
		public function ProjectLoader() 
		{	
			super( "SelectedWork" );
			stage.scaleMode = StageScaleMode.NO_SCALE;
			stage.align = StageAlign.TOP_LEFT;
			
			_backgroundGrad = new Sprite();
			_loaderStatus = new EmbeddedTextField( ConfigVariables.HELVETICA_NEUE_LT, 14, 0xFFFFFF, false, false, false );
			_animatedLoader = new LoadingBar();
			
			stage.addEventListener( Event.RESIZE, onResize );
			addChild( _backgroundGrad );
			
			
			var menu:BaseContextMenu = new BaseContextMenu( this );
			menu.addLink( "Ghostmonk Blog", "http://journal.ghostmonk.com" );
			menu.addLink( "Main Site (Experimental)", "http://www.ghostmonk.com" );	
		}
		
		override protected function updateLoader( percent:Number ) : void 
		{	
			_animatedLoader.gotoAndStop( Math.ceil( percent * _animatedLoader.totalFrames ) );	
		}
		
		private function onResize( e:Event ) : void 
		{	
			_animatedLoader.x = stage.stageWidth/2;
			_animatedLoader.y = stage.stageHeight/2;
			
			_backgroundGrad.graphics.clear();
			var backgroundGradientMatrix:Matrix = new Matrix();
			backgroundGradientMatrix.createGradientBox( stage.stageWidth, stage.stageHeight, Math.PI * 2.5, 0, 0 ); 
			_backgroundGrad.graphics.beginGradientFill( GradientType.LINEAR, [ 0x000000, 0x3663ac ], [ 0, 0.9 ], [ 50, 255 ], backgroundGradientMatrix, SpreadMethod.PAD );
			_backgroundGrad.graphics.drawRect( 0, 0, stage.stageWidth, stage.stageHeight );
			_backgroundGrad.graphics.endFill();
			
			if( _animatedLoader ) 
			{				
				_animatedLoader.x = ( stage.stageWidth - _animatedLoader.width ) * 0.5;
			  	_animatedLoader.y = ( stage.stageHeight - _animatedLoader.height ) * 0.5;
			}
			  	
		  	_loaderStatus.x = _animatedLoader.x;
		  	_loaderStatus.y = _animatedLoader.y - 25;	
		}
		
		private function handleOpen( e:Event ) : void  
		{	
			onResize( null );
			
			addChild( _animatedLoader );
		  	addChild( _loaderStatus );
		  	
		  	_animatedLoader.x = ( stage.stageWidth - _animatedLoader.width ) * 0.5;
		  	_animatedLoader.y = ( stage.stageHeight - _animatedLoader.height ) * 0.5;
		  
		  	_loaderStatus.text = "Loading: 0%";  	
		}
		
		private function handleProgress( e:ProgressEvent ) : void 
		{
		  var percent:Number = int( e.bytesLoaded / e.bytesTotal * 100 );
		  _animatedLoader.gotoAndStop( percent );
		  _loaderStatus.text = "Loading: " + percent + "%";  
		}
		
		private function handleComplete( e:Event ) : void 
		{	 
			TweenLite.to( _animatedLoader, 0.5, { alpha:0, ease:Quad.easeOut } );
			TweenLite.to( _loaderStatus, 0.5, { alpha:0, ease:Quad.easeOut, onComplete:applicationReady } );	
		} 
		
		private function applicationReady() : void 
		{	
			removeChild( _loaderStatus );
			removeChild( _animatedLoader );	
		}		
	}
}