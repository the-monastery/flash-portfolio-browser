package  
{	
	import caurina.transitions.properties.TextShortcuts;
	
	import com.ghostmonk.selectedWork.coreDisplay.Body;
	import com.ghostmonk.selectedWork.globals.ConfigVariables;
	import com.ghostmonk.selectedWork.utils.Scroller;
	import com.greensock.TweenLite;
	import com.greensock.easing.Quad;
	import com.greensock.plugins.AutoAlphaPlugin;
	import com.greensock.plugins.BezierPlugin;
	import com.greensock.plugins.FramePlugin;
	import com.greensock.plugins.TweenPlugin;
	
	import flash.display.Sprite;
	import flash.events.Event;
	
	[SWF (backgroundColor=0x000000, frameRate=31, width=1500, height=1100, pageTitle="Selected Work" )]
	[Frame ( factoryClass="com.ghostmonk.selectedWork.ProjectLoader" ) ]
	
	public class SelectedWork extends Sprite 
	{
		private var _body:Body;
		private var _scroller:Scroller;
		
		public function SelectedWork() 
		{	
			TextShortcuts.init();
			TweenLite.defaultEase = Quad.easeOut;
			TweenPlugin.activate( [ BezierPlugin, FramePlugin, AutoAlphaPlugin ] )
			
			_body = new Body();
			_scroller = new Scroller( _body );
			
			addEventListener( Event.ADDED_TO_STAGE, onAddedToStage );
		}
		
		private function onAddedToStage( e:Event ) : void 
		{	
			removeEventListener( Event.ADDED_TO_STAGE, onAddedToStage );
			addChild( _body );
			addChild( _scroller );
			stage.addEventListener( Event.RESIZE, onStageResize );
			onStageResize( null );	
		}
		
		private function onStageResize( e:Event ) : void 
		{	
			var xPos:Number = ( stage.stageWidth - ConfigVariables.BODY_WIDTH ) * 0.5;
			var bodyTween:TweenLite = new TweenLite( _body, 0.5, { x:xPos, y:0 } );
			
			if(stage.stageHeight < ConfigVariables.BODY_HEIGHT) 
			{
				_scroller.formatScroller();
			}
			else 
			{
				_scroller.destroyScroller();
			}
		}
	}
} 